<?php

namespace App\Controller;

use App\Entity\Posts;
use App\Form\PostsType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PostsController extends AbstractController
{
    /**
     * @Route("/registrar-posts", name="RegistrarPosts")
     */
    public function index(Request $request)
    {
        $post = new Posts();
        $form = $this->createForm(PostsType::class, $post);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

//          Capturamos el campo foto de nuestro formulario de subida de un Post
            $postFile = $form->get('foto')->getData();

            // this condition is needed because the 'brochure' field is not required
            // so the PDF file must be processed only when a file is uploaded
            if ($postFile) {
                $originalFilename = pathinfo($postFile->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
//                $safeFilename = $slugger->slug($originalFilename);
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9] remove; lower()', $originalFilename);
                $newFilename = $safeFilename . '-' . uniqid() . '.' . $postFile->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $postFile->move(
                        $this->getParameter('fotos_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    throw new \Exception('Error en la subida de la imagen');
                }

                // updates the 'postFilename' property to store the PDF file name
                // instead of its contents
                $post->setFoto($newFilename);
            }

            $user = $this->getUser();
            $post->setUser($user);

//          Entity manager sirve para buscar, consultar, guardar o eliminar registros de la base de datos
            $em = $this->getDoctrine()->getManager();
//          Guardamos el post
            $em->persist($post);
            $em->flush();

            return $this->redirectToRoute('dashboard');
        }

        return $this->render('posts/index.html.twig', [
            'controller_name' => 'PostsController',
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/post/{id}", name="VerPost")
     */
    public function verPost($id)
    {
//      Capturamos el id de la Ruta, lo recibimos por parametro y se lo pasamos al find()
        $em = $this->getDoctrine()->getManager();
        $post = $em->getRepository(Posts::class)->find($id);

        return $this->render('posts/verPost.html.twig', ['post' => $post]);
    }

    /**
     * @Route("/mis-post", name="MisPost")
     */
    public function misPost()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $posts = $em->getRepository(Posts::class)->findBy(['user' => $user]);

        return $this->render('posts/misPost.html.twig', ['post' => $posts]);
    }

//  Nuestra llamada AJAX, con el valor expose lo que obtenemos es que este expuesta a JS

    /**
     * @Route("/likes", options={"expose"=true}, name="likes")
     */
    public function Like(Request $request)
    {
//      Determinamos si es una llamada AJAX o HTTP
        if ($request->isXmlHttpRequest()) {

            $em = $this->getDoctrine()->getManager();
            $user = $this->getUser();

//      Tomamos el id del post que nos da el usuario a través de su click en la funcion AJAX (campo data)
            $id = $request->get('id');
            $post = $em->getRepository(Posts::class)->find($id);
            $likes = $post->getLikes();
            $likes .= $user->getId() . '.';
            $post->setLikes($likes);
            $em->flush();

            return new JsonResponse(['likes' => $likes]);

        } else {
            throw new \Exception('Estas tratando de joderme?');
        }
    }
}
