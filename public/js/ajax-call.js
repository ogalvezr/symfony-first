// Recibe como parametro el id del Post
function MeGusta(id) {

    // Le pasamos el Name que le asignamos a la funcion del controlador
    var Ruta = Routing.generate('likes');

    $.ajax({
        type: 'POST',
        url: Ruta,
        data: ({id: id}),
        async: true,
        dataType: "json",
        success: function (data) {
            // console.log(data['likes']);
            /** TODO: Detectar que si el ID del usuario está entre los likes, que lo ponga automáticamente
             * cargandolo desde JS , y no recargando la página
            **/
            window.location.reload();
        }
    });
}